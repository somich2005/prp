#include <stdio.h>
#include <stdlib.h>
#define ALPHABET_SIZE 52

void get_alphabet(char array[ALPHABET_SIZE])
{
    int counter = 0;
    char symb = 'a';
    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        array[i] = symb;
        symb++;
        counter++;
        if (i == 25)
            symb = 'A';
            printf("%c", array[i]);
    }
    printf("%d", counter);
}

int main(){

    char a[52];
    get_alphabet(a);
    return 0;
}