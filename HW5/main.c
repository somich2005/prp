#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Define constants
#define BUFFERSIZE 10
#define OUT_OF_ALPHABET 100
#define WRONG_LENGTH 101
#define ALPHABET_SIZE 52

// Function declarations
char *get_message(char *message);
void get_alphabet(char array[ALPHABET_SIZE]);
int does_in_alpha(char *message, char *alphabet);

char *shift(const char *str, char *alphabet, int value, int str_len, char result[str_len + 1]);
int compare(const char *str1, const char *str2);
char *rotate(char *original, char *message, char *alphabet, char *ptr);

int main()
{
    // allocate memory for messages
    char *cesar = malloc(sizeof(char) * BUFFERSIZE);
    char *message = malloc(sizeof(char) * BUFFERSIZE);

    // reading messages
    cesar = get_message(cesar);
    message = get_message(message);

    // correct input checking
    char alphabet[ALPHABET_SIZE];
    get_alphabet(alphabet);

    // Check if messages contain valid characters
    if (does_in_alpha(cesar, alphabet) == OUT_OF_ALPHABET || does_in_alpha(message, alphabet) == OUT_OF_ALPHABET)
    {
        free(cesar);
        free(message);
        fprintf(stderr, "Error: Chybny vstup!\n");
        return OUT_OF_ALPHABET;
    }
    else if (strlen(message) != strlen(cesar))
    {
        free(cesar);
        free(message);
        fprintf(stderr, "Error: Chybna delka vstupu!\n");
        return WRONG_LENGTH;
    }

    // memory allocation for final answer
    int cesar_len = strlen(cesar);
    char *result = malloc(sizeof(char) * (cesar_len + 1));
    result = rotate(cesar, message, alphabet, result);

    printf("%s\n", result);

    // Free allocated memory
    free(cesar);
    free(message);
    free(result);
    return 0;
}

// Function to dynamically allocate memory for a message
char *get_message(char *message)
{
    int bufferSize = BUFFERSIZE;
    int position = 0;
    int c;
    while ((c = getchar()) != '\n')
    {
        if (position == bufferSize - 1)
        {
            bufferSize *= 2;
            message = (char *)realloc(message, bufferSize * sizeof(char));
        }
        message[position++] = c;
    }
    message[position] = '\0';
    return message;
}

// Function to get the alphabet
void get_alphabet(char array[ALPHABET_SIZE])
{
    char symb = 'a';
    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        array[i] = symb;
        symb++;
        if (i == 25)
            symb = 'A';
    }
}

// Function to check if all characters in the message are in the alphabet
int does_in_alpha(char *message, char *alphabet)
{
    int len = strlen(message);
    int flag;
    for (int i = 0; i < len; ++i)
    {
        flag = 0;
        for (int j = 0; j < ALPHABET_SIZE; j++)
        {
            if (message[i] == alphabet[j])
            {
                flag = 1;
                break;
            }
        }
        if (flag == 0)
        {
            return OUT_OF_ALPHABET;
        }
    }
    return 0; // in case every symbol is in the alphabet
}

// Function to shift characters in a string based on an alphabet
char *shift(const char *str, char *alphabet, int value, int str_len, char result[str_len + 1])
{
    int alphabet_len = ALPHABET_SIZE;
    for (int i = 0; i < str_len; ++i)
    {
        for (int j = 0; j < alphabet_len; ++j)
        {
            if (str[i] == alphabet[j])
            {
                result[i] = alphabet[((j + value + 1) % ALPHABET_SIZE)];
                break;
            }
        }
    }
    result[str_len] = '\0';
    return result;
}

// Function to compare two strings and count matching characters
int compare(const char *str1, const char *str2)
{
    int counter = 0;
    int len = strlen(str1);
    for (int i = 0; i < len; i++)
    {
        if (str1[i] == str2[i])
            counter++;
    }
    return counter;
}

// Function to find the best rotation for decryption
char *rotate(char *original, char *message, char *alphabet, char *ptr)
{
    int alphabet_len = ALPHABET_SIZE;
    int original_len = strlen(original);
    int answers[ALPHABET_SIZE];
    // Try all possible rotations and count matching characters
    for (int i = 0; i < alphabet_len; ++i)
    {
        ptr = shift(original, alphabet, i, original_len, ptr);
        answers[i] = compare(ptr, message);
    }

    // Find the rotation with the maximum matching characters
    int max_value = answers[0];
    int index;
    for (int i = 0; i < alphabet_len; i++)
    {
        if (answers[i] > max_value)
        {
            max_value = answers[i];
            index = i;
        }
    }
    // Decrypt the message using the best rotation
    ptr = shift(original, alphabet, index, original_len, ptr);
    return ptr;
}
